/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "lpus.h"

/**
 * Example program: how to decode "function" TC and encode "ack" TM
 *
 * Usage:
 *
 * $ ./lpussleep 2
 * Decoded: TC0801, func: sleep, arg[0]: 2, requires ack: start progress complete
 * Encoded: TM0103 ack successful start
 * Encoded: TM0105 ack successful progress, step 0
 * Encoded: TM0105 ack successful progress, step 1
 * Encoded: TM0107 ack successful complete
 */
int main(int argc, char* argv[])
{
    uint8_t received_space_packet[] = {
        /* space packet primary header */
        0x5F, 0x55, 0xC0, 0x01, 0x00, 0x19,
        /* space packet payload */
        0x27, 0x08, 0x01, 0x00, 0x00, 0x73, 0x6C, 0x65,
        0x65, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00 /* func arg[0] */,
        0x00, 0x00
    };
    const size_t space_packet_hdr_size = 6;
    uint8_t* received_payload = &received_space_packet[space_packet_hdr_size];
    size_t payload_size = sizeof(received_space_packet) -
                          space_packet_hdr_size;

    /* first argument should be number of seconds to sleep */
    if (argc >= 2) {
        unsigned long sleep_sec = strtoul(argv[1], NULL, 0);

        /* save as func arg[0] in received packet payload */
        received_payload[23] = (uint8_t)sleep_sec;
    }

    /* Step 1. Decode received packet payload */

    uint16_t subservice = 0;
    lpus_struct_t tc_lpus_struct = { };
    lpus_struct_t tm_lpus_struct = { };
    uint8_t buf[64] = { };

    int32_t ret = lpus_decode(received_payload, payload_size,
                              &subservice, &tc_lpus_struct);
    if (ret < 0) {
        errno = (-ret);
        err(1, "failed to decode PUS message");
    }
    if (subservice != TC0801_FUNC) {
        errno = EINVAL;
        err(1, "decoded unsupported PUS subservice");
    }
    if (strcmp(tc_lpus_struct.st08.tc0801.func_id, "sleep")) {
        errno = EINVAL;
        err(1, "decoded unsupported function");
    }

    /* mission-specific argument structure for "sleep" function */
    struct __attribute__((packed)) {
        uint8_t arg_id;
        uint8_t arg_val;
    }
    * sleep_args = tc_lpus_struct.st08.tc0801.args;

    printf("Decoded: TC0801, func: %s, arg[%u]: %u, requires ack: %s%s%s%s\n",
           tc_lpus_struct.st08.tc0801.func_id,
           sleep_args->arg_id, sleep_args->arg_val,
           (tc_lpus_struct.st08.tc0801.tc_ack.flags.accept_request ?
            "accept " : ""),
           (tc_lpus_struct.st08.tc0801.tc_ack.flags.start_exec ?
            "start " : ""),
           (tc_lpus_struct.st08.tc0801.tc_ack.flags.progress_exec ?
            "progress " : ""),
           (tc_lpus_struct.st08.tc0801.tc_ack.flags.complete_exec ?
            "complete " : "")
          );

    /* Step 2. Encode response "successful start of execution" */

    if (tc_lpus_struct.st08.tc0801.tc_ack.flags.start_exec) {
        subservice = TM0103_SUCCESS_START;
        tm_lpus_struct.st01.tm0103.tc_req_id = received_space_packet;

        ret = lpus_encode(subservice, &tm_lpus_struct, buf, sizeof(buf));
        if (ret < 0) {
            errno = (-ret);
            err(1, "failed to encode PUS message TM0103");
        }

        printf("Encoded: TM0103 ack successful start\n");
    }

    /* Step 3. Encode response "successful progress of execution" */

    for (uint16_t i = 0; i < sleep_args->arg_val; i++) {
        if (tc_lpus_struct.st08.tc0801.tc_ack.flags.progress_exec) {
            subservice = TM0105_SUCCESS_PROGRESS;
            tm_lpus_struct.st01.tm0105.tc_req_id = received_space_packet;
            tm_lpus_struct.st01.tm0105.step = i;

            ret = lpus_encode(subservice, &tm_lpus_struct, buf, sizeof(buf));
            if (ret < 0) {
                errno = (-ret);
                err(1, "failed to encode PUS message TM0105");
            }

            printf("Encoded: TM0105 ack successful progress, step %u\n", i);
        }

        /* actual sleep on every step */
        sleep(1);
    }

    /* Step 4. Encode response "successful complete of execution" */

    if (tc_lpus_struct.st08.tc0801.tc_ack.flags.complete_exec) {
        subservice = TM0107_SUCCESS_COMPLETE;
        tm_lpus_struct.st01.tm0107.tc_req_id = received_space_packet;

        ret = lpus_encode(subservice, &tm_lpus_struct, buf, sizeof(buf));
        if (ret < 0) {
            errno = (-ret);
            err(1, "failed to encode PUS message TM0107");
        }

        printf("Encoded: TM0107 ack successful complete\n");
    }

    return 0;
}
