/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <stdio.h>
#include <string.h>

#include "lpus.h"

/**
 * Example program: how to encode PUS TM[5,1] informative event report.
 *
 * Usage:
 *
 * $ ./lpusinfo hello
 * 2005010000000000000000000000002168656C6C6F
 *
 * $ ./lpusinfo "too very long message"
 * pusinfo: failed to encode PUS message: File too large
 */
int main(int argc, char* argv[])
{
    /* first argument should be a message to encode */
    if (argc < 2) {
        errno = EINVAL;
        err(1, "no message to encode");
    }

    /* prepare input structure for TM[5,1] */
    uint16_t subservice = TM0501_EV_INFO;
    lpus_struct_t lpus_struct = { };
    lpus_struct.st05.tm0501.ev_id = 0x21; /* mission-specific event ID */
    lpus_struct.st05.tm0501.data = argv[1];
    lpus_struct.st05.tm0501.data_size = strlen(argv[1]);

    /* prepare output buffer for PUS payload */
    /* for the purpose of this example, output buffer is limited to 32 bytes */
    uint8_t buf[32] = { };

    /* encode */
    int32_t ret = lpus_encode(subservice, &lpus_struct, buf, sizeof(buf));
    if (ret < 0) {
        errno = (-ret);
        err(1, "failed to encode PUS message");
    }

    /* print result */
    for (int32_t i = 0; i < ret; i++) {
        printf("%02X", buf[i]);
    }

    printf("\n");

    return 0;
}
