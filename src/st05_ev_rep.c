/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "st05_ev_rep_priv.h"

static int32_t st05_encode_ev_rep(tm05xx_ev_rep_t* in_rep, uint8_t* out_buf,
                                  size_t buf_size)
{
    int32_t out_size;

    /* invalid arguments */
    if (!in_rep->data)
        return (-EINVAL);

    /* size of data to write to out_buf */
    out_size = sizeof(event_id_t) + in_rep->data_size;

    /* data too large for a given out_buf */
    if (out_size > (int32_t)buf_size)
        return (-EFBIG);

    /* FIXME: properly encode field sizes */
    memcpy(out_buf, &in_rep->ev_id, sizeof(event_id_t));
    memcpy((out_buf + sizeof(event_id_t)), in_rep->data, in_rep->data_size);

    return out_size;
}

static int32_t st05_decode_ev_rep(uint8_t* in_buf, size_t data_size,
                                  tm05xx_ev_rep_t* out_rep)
{
    /* data too small */
    if (data_size < sizeof(event_id_t))
        return (-EINVAL);

    /* FIXME: properly encode field sizes */
    memcpy(&out_rep->ev_id, in_buf, sizeof(event_id_t));
    out_rep->data = in_buf + sizeof(event_id_t);
    out_rep->data_size = data_size - sizeof(event_id_t);

    return 0;
}

static int32_t st05_encode_ev_list(tc05xx_ev_list_t* in_list, uint8_t* out_buf,
                                   size_t buf_size)
{
    int32_t out_size;

    /* invalid arguments */
    if (!in_list->ev_list)
        return (-EINVAL);

    /* size of data to write to out_buf */
    out_size = sizeof(in_list->num) + (sizeof(event_id_t) * in_list->num);

    /* data too large for a given out_buf */
    if (out_size > (int32_t)buf_size)
        return (-EFBIG);

    memcpy(out_buf, &in_list->num, sizeof(in_list->num));
    memcpy((out_buf + sizeof(in_list->num)), in_list->ev_list,
           (sizeof(event_id_t) * in_list->num));

    return out_size;
}

static int32_t st05_decode_ev_list(uint8_t* in_buf, size_t data_size,
                                   tc05xx_ev_list_t* out_list)
{
    size_t exp_size;

    /* data too small */
    if (data_size < sizeof(out_list->num))
        return (-EINVAL);

    memcpy(&out_list->num, in_buf, sizeof(out_list->num));

    exp_size = sizeof(out_list->num) + (sizeof(event_id_t) * out_list->num);

    /* data too small */
    if (data_size < exp_size)
        return (-EINVAL);

    out_list->ev_list = in_buf + sizeof(out_list->num);

    return 0;
}

bool st05_is_tc(st05_subservice_t in_sub, st05_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack)
{
    bool is_tc = false;
    lpus_tc_ack_t* tc_ack_p = NULL;

    switch (in_sub) {
        case TC0505_EN_REP:
            tc_ack_p = &in_struct->tc0505.tc_ack;
            is_tc = true;
            break;

        case TC0506_DIS_REP:
            tc_ack_p = &in_struct->tc0506.tc_ack;
            is_tc = true;
            break;

        case TC0507_REQ_DIS_LIST:
            tc_ack_p = &in_struct->tc0507.tc_ack;
            is_tc = true;
            break;

        default:
            break;
    }

    if (out_tc_ack)
        *out_tc_ack = tc_ack_p;

    return is_tc;
}

int32_t st05_encode(st05_subservice_t in_sub, st05_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size)
{
    switch (in_sub) {
        case TM0501_EV_INFO:
            return st05_encode_ev_rep(&in_struct->tm0501, out_buf, buf_size);

        case TM0502_EV_LOW:
            return st05_encode_ev_rep(&in_struct->tm0502, out_buf, buf_size);

        case TM0503_EV_MEDIUM:
            return st05_encode_ev_rep(&in_struct->tm0503, out_buf, buf_size);

        case TM0504_EV_HIGH:
            return st05_encode_ev_rep(&in_struct->tm0504, out_buf, buf_size);

        case TC0505_EN_REP:
            return st05_encode_ev_list(&in_struct->tc0505, out_buf, buf_size);

        case TC0506_DIS_REP:
            return st05_encode_ev_list(&in_struct->tc0506, out_buf, buf_size);

        case TC0507_REQ_DIS_LIST:
            return 0; /* no parameters to encode */

        case TM0508_RESP_DIS_LIST:
            return st05_encode_ev_list(&in_struct->tm0508, out_buf, buf_size);

        default:
            break;
    }

    return (-EINVAL);
}

int32_t st05_decode(st05_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st05_struct_t* out_struct)
{
    switch (in_sub) {
        case TM0501_EV_INFO:
            return st05_decode_ev_rep(in_buf, data_size, &out_struct->tm0501);

        case TM0502_EV_LOW:
            return st05_decode_ev_rep(in_buf, data_size, &out_struct->tm0502);

        case TM0503_EV_MEDIUM:
            return st05_decode_ev_rep(in_buf, data_size, &out_struct->tm0503);

        case TM0504_EV_HIGH:
            return st05_decode_ev_rep(in_buf, data_size, &out_struct->tm0504);

        case TC0505_EN_REP:
            return st05_decode_ev_list(in_buf, data_size, &out_struct->tc0505);

        case TC0506_DIS_REP:
            return st05_decode_ev_list(in_buf, data_size, &out_struct->tc0506);

        case TC0507_REQ_DIS_LIST:
            return 0; /* no parameters to decode */

        case TM0508_RESP_DIS_LIST:
            return st05_decode_ev_list(in_buf, data_size, &out_struct->tm0508);

        default:
            break;
    }

    return (-EINVAL);
}
