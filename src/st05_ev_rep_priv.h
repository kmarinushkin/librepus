/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Service type ST[05] event reporting
 * Private header
 */

#ifndef SRC_ST05_EV_REP_PRIV_H_
#define SRC_ST05_EV_REP_PRIV_H_

#include <stdbool.h>

#include "lpus_common.h"
#include "st05_ev_rep.h"

/* Is suservice TC or not (TM) */
bool st05_is_tc(st05_subservice_t in_sub, st05_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack);

/* Encode ST[05] event report */
int32_t st05_encode(st05_subservice_t in_sub, st05_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size);

/* Decode ST[05] event report */
int32_t st05_decode(st05_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st05_struct_t* out_struct);

#endif /* SRC_ST05_EV_REP_PRIV_H_ */
